# Predicting Airplane Delays

This Repository simply contains a baseline modelling to predict flight delays due to weather

### The Business Problem
Business people are really busy people often time everything, they allocate their time for certain events and follow thier calendar schedule judiciously, however delayed flights may often disrupt thier calendar, spoil their plans or give them some time to do things they didn't know they would have had the time to do.

### Business Goal
The ability to effectively estimate if a flight will be delayed due to weather is a great advantage, as it means that people would be able to take note of that before even booking a flight, and would help guide their timing decisions for that day.

### Running the notebook.
The notebook is quite computationally expensive as it requires high ram to effectively load in and manipulate the data.

To run it, you simply need to download the data and put it in the root directory. The notebook also runs on google colab effectively.